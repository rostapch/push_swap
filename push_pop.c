/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_pop.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:28:59 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:29:01 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int		ft_push(t_stack *stk, int nb)
{
	if (!(stk && stk->pos < stk->len - 1))
		return (0);
	stk->data[++stk->pos] = nb;
	return (1);
}

int		ft_pop(t_stack *stk, int *nb)
{
	if (!(stk && stk->pos >= 0))
		return (0);
	*nb = stk->data[stk->pos--];
	return (1);
}
