/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:26:26 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:26:28 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

#define A (ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos - 2])
#define AA (ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos])
#define B (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 1])
#define BB (ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos - 2])
#define C (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 2])
#define CC (ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos - 1])
#define D (ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos])
#define DD (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 2])
#define E (ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos])
#define EE (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 1])

void	sort_only_3_a(t_ps *ps)
{
	if (ps->a.pos != 2)
		sort_2_a(ps);
	else if (ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 1] &&
		ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos - 2])
	{
		ft_sa(ps);
		ft_rra(ps);
	}
	else if (ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 2] &&
		ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos - 1])
		ft_ra(ps);
	else if (ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos] &&
		ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 2])
		ft_rra(ps);
	else if (ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos] &&
		ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 1])
		ft_sa(ps);
	else if (ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos - 2] &&
		ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos])
	{
		ft_sa(ps);
		ft_ra(ps);
	}
}

void		triple_push(t_ps *ps)
{
	ft_pa(ps);
	ft_pa(ps);
	ft_pa(ps);
}

void	sort_only_3_b(t_ps *ps, int len)
{
	if (len == 3 && ps->b.pos == 2)
	{
		if (A && AA)
		{
			ft_pb(ps);
			ft_sb(ps);
			double_push(ps);
			return ;
		}
		if (B && BB)
		{
			ft_sb(ps);
			ft_rrb(ps);
		}
		else if (C && CC)
			ft_rb(ps);
		else if (D && DD)
			ft_rrb(ps);
		else if (E && EE)
			ft_sb(ps);
		triple_push(ps);
	}
	else
		sort_2_b(ps, len);
}
