/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnome_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:30:38 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:30:40 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void	gnome_sort(int *array, int size)
{
	int i;
	int tmp;

	i = 1;
	while (i < size)
	{
		if (array[i - 1] >= array[i])
			++i;
		else
		{
			tmp = array[i];
			array[i] = array[i - 1];
			array[i - 1] = tmp;
			--i;
			if (i == 0)
				i = 1;
		}
	}
}

int		partition(int arr[], int low, int high)
{
	int pivot;
	int temp;
	int i;
	int j;

	pivot = arr[high];
	i = (low - 1);
	j = low - 1;
	while (++j <= high - 1)
		if (arr[j] <= pivot)
		{
			i++;
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
	temp = arr[i + 1];
	arr[i + 1] = arr[high];
	arr[high] = temp;
	return (i + 1);
}

void	quicksort(int *arr, int low, int high)
{
	int pi;

	if (low < high)
	{
		pi = partition(arr, low, high);
		quicksort(arr, low, pi - 1);
		quicksort(arr, pi + 1, high);
	}
}
