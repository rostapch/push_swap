/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   med.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:30:11 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:30:13 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int	ft_med(t_stack stk, int sz)
{
	int *arr;
	int i;
	int med;

	if (!(arr = (int*)ft_memalloc(sizeof(int) * (sz))))
		return (0);
	i = -1;
	while (++i < sz)
		arr[i] = stk.data[stk.pos - i];
	quicksort(arr, 0, sz - 1);
	med = sz % 2 ? arr[sz / 2] : arr[sz / 2 - 1];
	ft_memdel((void**)&arr);
	return (med);
}
