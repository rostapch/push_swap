/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_ps.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:31:24 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:31:27 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int	ft_check_dup(t_stack *st)
{
	int	i;
	int j;

	i = -1;
	while (++i < st->len - 1)
	{
		j = i;
		while (++j < st->len)
			if (st->data[i] == st->data[j])
				return (0);
	}
	return (1);
}

int	ft_fill_ps(t_ps *ps, char **arg)
{
	while (++ps->a.pos < ps->a.len)
		ps->a.data[ps->a.len - 1 - ps->a.pos] = ft_atoi(arg[ps->a.pos + 1]);
	ps->a.pos--;
	return (ft_check_dup(&ps->a));
}
