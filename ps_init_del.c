/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_init_del.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:29:19 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:29:21 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

t_ps	*del_ps(t_ps *ps)
{
	if (!ps)
		return (ps);
	ft_memdel((void **)&ps->a.data);
	ft_memdel((void **)&ps->b.data);
	ft_memdel((void **)&ps->act);
	return (ps);
}

t_ps	*init_ps(t_ps *ps, int length)
{
	if (!(ps->a.data = (int *)malloc(sizeof(int) * length)))
		return (NULL);
	if (!(ps->b.data = (int *)malloc(sizeof(int) * length)))
		return (del_ps(ps));
	if (!(ps->act = ft_strnew(0)))
		return (del_ps(ps));
	ps->a.len = length;
	ps->b.len = length;
	ps->a.pos = -1;
	ps->b.pos = -1;
	return (ps);
}
