/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_stcs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/16 18:49:53 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/16 18:49:54 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void	ft_rotate_a(t_ps *ps, int rotates, int len)
{
	int	i;

	if (rotates < ps->a.pos + 1)
	{
		i = -1;
		while (++i < rotates)
			ft_rra(ps);
	}
	else
	{
		i = rotates - 1;
		while (++i < len - rotates)
			ft_ra(ps);
	}
}

void	ft_rotate_b(t_ps *ps, int rotates, int len)
{
	int	i;

	if (rotates < ps->b.pos + 1)
	{
		i = -1;
		while (++i < rotates)
			ft_rrb(ps);
	}
	else
	{
		i = rotates - 1;
		while (++i < len - rotates)
			ft_rb(ps);
	}
}
