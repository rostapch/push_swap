/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:34:34 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:34:36 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int		ft_sorted(t_ps *ps)
{
	int i;

	if (!(ps->a.len > 0 && ps->a.pos == ps->a.len - 1 && ps->b.pos < 0))
		return (0);
	i = -1;
	while (++i < ps->a.len - 1)
		if (ps->a.data[i] < ps->a.data[i + 1])
			return (0);
	return (1);
}

int		spec(t_ps *ps, char **line)
{
	if (ft_strcmp("ss", *line) == 0)
		ft_ss(ps);
	else if (ft_strcmp("rr", *line) == 0)
		ft_rr(ps);
	else if (ft_strcmp("rrr", *line) == 0)
		ft_rrr(ps);
	else
		return (0);
	return (1);
}

void	ft_check_args(t_ps *ps, char **line)
{
	if (ft_strcmp("sa", *line) == 0)
		ft_sa(ps);
	else if (ft_strcmp("sb", *line) == 0)
		ft_sb(ps);
	else if (ft_strcmp("pa", *line) == 0)
		ft_pa(ps);
	else if (ft_strcmp("pb", *line) == 0)
		ft_pb(ps);
	else if (ft_strcmp("ra", *line) == 0)
		ft_ra(ps);
	else if (ft_strcmp("rb", *line) == 0)
		ft_rb(ps);
	else if (ft_strcmp("rra", *line) == 0)
		ft_rra(ps);
	else if (ft_strcmp("rrb", *line) == 0)
		ft_rrb(ps);
	else if (!spec(ps, line))
	{
		ft_memdel((void *)line);
		ft_error(ps);
	}
	ft_memdel((void *)line);
}
