/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_main.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:28:37 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:28:39 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void	rep(t_ps *ps)
{
	ps->act = str_replace(ps->act, "\nsa\nsa\n", "\n");
	ps->act = str_replace(ps->act, "\nsb\nsb\n", "\n");
	ps->act = str_replace(ps->act, "\nsa\nsb\n", "\nss\n");
	ps->act = str_replace(ps->act, "\nra\nrra\n", "\n");
	ps->act = str_replace(ps->act, "\nrra\nra\n", "\n");
	ps->act = str_replace(ps->act, "\nrb\nrrb\n", "\n");
	ps->act = str_replace(ps->act, "\nrrb\nrb\n", "\n");
	ps->act = str_replace(ps->act, "\npb\npa\n", "\n");
	ps->act = str_replace(ps->act, "\npa\npb\n", "\n");
	ps->act = str_replace(ps->act, "\nra\nrb\n", "\nrr\n");
	ps->act = str_replace(ps->act, "\nrb\nra\n", "\nrr\n");
	ps->act = str_replace(ps->act, "\nrra\nrrb\n", "\nrrr\n");
	ps->act = str_replace(ps->act, "\nrrb\nrra\n", "\nrrr\n");
}

int		main(int argc, char **argv)
{
	t_ps	ps;

	if (argc == 1)
		return (ft_error(NULL));
	if (!ft_is_valid(argc, argv))
		return (ft_error(NULL));
	if (!init_ps(&ps, argc - 1))
		return (ft_error(NULL));
	if (!ft_fill_ps(&ps, argv))
		return (ft_error(&ps));
	if (!ft_sorted(&ps))
		sort(&ps, 0, ps.a.len);
	rep(&ps);
	ft_putstr(ps.act);
	del_ps(&ps);
	return (0);
}
