/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:27:23 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:27:30 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void	sort_2_a(t_ps *ps)
{
	if (ps->a.pos >= 1)
	{
		if (ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 1])
			ft_sa(ps);
	}
}

void	sort_2_b(t_ps *ps)
{
	if (ps->b.pos >= 1)
	{
		if (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 1])
			ft_sb(ps);
	}
}
