/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:30:00 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:30:02 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_PS_H
# define PUSH_SWAP_PS_H

# include <stdlib.h>
# include <unistd.h>
# include "libft/libft.h"
# include "libft/get_next_line.h"

typedef struct		s_stack
{
	int				*data;
	int				len;
	int				pos;
}					t_stack;

typedef struct		s_ps
{
	t_stack			a;
	t_stack			b;
	char			*act;
}					t_ps;

int					ft_error(t_ps *ps);
int					ft_sorted(t_ps *ps);

void				ft_check_args(t_ps *ps, char **line);
int					ft_check_all(t_ps *ps, char **line);
int					ft_is_valid(int len, char **args);
int					ft_fill_ps(t_ps *ps, char **arg);
t_ps				*init_ps(t_ps *ps, int length);
t_ps				*del_ps(t_ps *ps);

int					ft_push(t_stack *stk, int nb);
int					ft_pop(t_stack *stk, int *nb);

void				ft_sa(t_ps *ps);
void				ft_sb(t_ps *ps);
void				ft_ss(t_ps *ps);

void				ft_pa(t_ps *ps);
void				ft_pb(t_ps *ps);

void				ft_ra(t_ps *ps);
void				ft_rb(t_ps *ps);
void				ft_rr(t_ps *ps);

void				ft_rra(t_ps *ps);
void				ft_rrb(t_ps *ps);
void				ft_rrr(t_ps *ps);

void				sort(t_ps *ps, int flag, int len);

void				sort_3_a(t_ps *ps);
void				sort_3_b(t_ps *ps);
void				sort_only_3_a(t_ps *ps);
void				sort_only_3_b(t_ps *ps);
void				sort_2_a(t_ps *ps);
void				sort_2_b(t_ps *ps);

void				gnome_sort(int *array, int size);
void				quicksort(int *arr, int low, int high);
int					ft_med(t_stack stk, int sz);
char				*str_replace(char *str, char *sub, char *rep);

#endif
