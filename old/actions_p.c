/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions_p.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:37:33 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:37:34 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void	ft_pa(t_ps *ps)
{
	int nb;

	if (ft_pop(&ps->b, &nb) && ft_push(&ps->a, nb))
		ps->act = ft_strjoin_free(ps->act, "pa\n");
}

void	ft_pb(t_ps *ps)
{
	int nb;

	if (ft_pop(&ps->a, &nb) && ft_push(&ps->b, nb))
		ps->act = ft_strjoin_free(ps->act, "pb\n");
}
