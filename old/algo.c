/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:35:16 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:35:18 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void	split_a(t_ps *ps, int len)
{
	int med;
	int rotates;
	int i;

	rotates = 0;
	i = -1;
	med = ft_med(ps->a, len);
	while (++i < len)
	{
		if (ps->a.data[ps->a.pos] < med)
			ft_pb(ps);
		else
		{
			rotates++;
			ft_ra(ps);
		}
	}
	i = -1;
	while (++i < rotates)
		ft_rra(ps);
	sort(ps, 0, rotates);
	sort(ps, 1, len - rotates);
}

void	split_b(t_ps *ps, int len)
{
	int med;
	int rotates;
	int i;

	rotates = 0;
	i = -1;
	med = ft_med(ps->b, len);
	while (++i < len)
	{
		if (ps->b.data[ps->b.pos] > med)
			ft_pa(ps);
		else
		{
			rotates++;
			ft_rb(ps);
		}
	}
	i = -1;
	while (++i < rotates)
		ft_rrb(ps);
	sort(ps, 0, len - rotates);
	sort(ps, 1, rotates);
}

void	split(t_ps *ps, int flag, int len)
{
	if (flag == 0)
		split_a(ps, len);
	else if (flag == 1)
		split_b(ps, len);
}

void	sort(t_ps *ps, int flag, int len)
{
	int i;

	if (flag == 0 && len <= 3 && len > -1)
	{
		sort_3_a(ps);
	}
	else if (flag == 1 && len <= 3 && len > -1)
	{
		sort_3_b(ps);
		i = -1;
		while (++i < len)
			ft_pa(ps);
	}
	else
		split(ps, flag, len);
}
