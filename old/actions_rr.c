/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions_rr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:36:20 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:36:21 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int		ft_rrotate(t_stack *stk)
{
	int i;
	int buff;

	if (!(stk && stk->pos > 0))
		return (0);
	i = -1;
	buff = stk->data[0];
	while (++i < stk->pos)
		stk->data[i] = stk->data[i + 1];
	stk->data[stk->pos] = buff;
	return (1);
}

void	ft_rra(t_ps *ps)
{
	if (ft_rrotate(&ps->a))
		ps->act = ft_strjoin_free(ps->act, "rra\n");
}

void	ft_rrb(t_ps *ps)
{
	if (ft_rrotate(&ps->b))
		ps->act = ft_strjoin_free(ps->act, "rrb\n");
}

void	ft_rrr(t_ps *ps)
{
	if (ft_rrotate(&ps->b) && ft_rrotate(&ps->a))
		ps->act = ft_strjoin_free(ps->act, "rrr\n");
}
