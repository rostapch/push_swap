/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:31:41 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:31:45 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

#define SZ (ft_strlen(old) - ft_strlen(sub) + ft_strlen(rep) + 1)
#define N_TO (new + (tok - old) + ft_strlen(rep))
#define O_S_TO (ft_strlen(old) - ft_strlen(sub) - (tok - old))

int		ft_error(t_ps *ps)
{
	del_ps(ps);
	ft_putstr_fd("Error\n", 2);
	exit(1);
	return (1);
}

char	*str_replace(char *str, char *sub, char *rep)
{
	char *tok;
	char *new;
	char *old;

	if (sub == NULL || rep == NULL)
		return (str);
	new = ft_strdup(str);
	while ((tok = ft_strstr(new, sub)))
	{
		old = new;
		new = ft_memalloc(SZ);
		ft_memset(new, 0, SZ);
		if (new == NULL)
		{
			free(old);
			return (NULL);
		}
		ft_memcpy(new, old, tok - old);
		ft_memcpy(new + (tok - old), rep, ft_strlen(rep));
		ft_memcpy(N_TO, tok + ft_strlen(sub), O_S_TO);
		ft_memset(new + ft_strlen(old) - ft_strlen(sub) + ft_strlen(rep), 0, 1);
		free(old);
	}
	free(str);
	return (new);
}
