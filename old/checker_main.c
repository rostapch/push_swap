/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_main.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:34:15 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:34:17 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int			main(int argc, char **argv)
{
	t_ps	ps;
	char	*line;

	if (argc == 1)
		return (ft_error(NULL));
	if (!ft_is_valid(argc, argv))
		return (ft_error(NULL));
	if (!init_ps(&ps, argc - 1))
		return (ft_error(NULL));
	if (!ft_fill_ps(&ps, argv))
		return (ft_error(&ps));
	while (ft_get_next_line(0, &line))
		ft_check_args(&ps, &line);
	if (ft_sorted(&ps))
		ft_putstr("OK\n");
	else
		ft_putstr("KO\n");
	del_ps(&ps);
	return (0);
}
