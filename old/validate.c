/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:23:39 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:23:41 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"
#include <limits.h>

int			ft_isnum(char *str)
{
	if (!str)
		return (0);
	if (ft_strchr("+-", *str))
		str++;
	while (*str)
		if (!ft_isdigit(*str++))
			return (0);
	return (1);
}

long long	ft_bla(const char *str)
{
	char		*p;
	int			i;
	long long	num;

	if (!(p = skip_blank(str)))
		return (0);
	num = 0;
	i = ((*p == '-') || (*p == '+')) ? 0 : -1;
	while (ft_isdigit(p[++i]))
		num = num * 10 + (int)(p[i] - '0');
	return ((*p == '-') ? -num : num);
}

int			ft_is_int(char *str)
{
	long long check;

	check = ft_bla((const char *)str);
	return (INT_MIN <= check && check <= INT_MAX);
}

int			ft_is_valid(int len, char **args)
{
	while (--len)
		if (!(ft_isnum(args[len]) && ft_is_int(args[len])))
			return (0);
	return (1);
}
