/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:28:15 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:28:16 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void		sort_a(t_ps *ps)
{
	if (ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos] &&
		ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 2])
	{
		ft_ra(ps);
		ft_sa(ps);
		ft_rra(ps);
		ft_sa(ps);
	}
	else if (ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos] &&
		ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 1])
		ft_sa(ps);
	else if (ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos - 2] &&
		ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos])
	{
		ft_ra(ps);
		ft_sa(ps);
		ft_rra(ps);
	}
}

void		sort_3_a(t_ps *ps)
{
	if (ps->a.pos > 2)
	{
		if (ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 1] &&
			ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos - 2])
		{
			ft_sa(ps);
			ft_ra(ps);
			ft_sa(ps);
			ft_rra(ps);
			ft_sa(ps);
		}
		else if (ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 2] &&
			ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos - 1])
		{
			ft_sa(ps);
			ft_ra(ps);
			ft_sa(ps);
			ft_rra(ps);
		}
		else
			sort_a(ps);
	}
	else
		sort_only_3_a(ps);
}

void		sort_b(t_ps *ps)
{
	if (ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos] &&
		ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 2])
	{
		ft_rb(ps);
		ft_sb(ps);
		ft_rrb(ps);
		ft_sb(ps);
	}
	else if (ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos] &&
		ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 1])
		ft_sb(ps);
	else if (ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos - 2] &&
		ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos])
	{
		ft_rb(ps);
		ft_sb(ps);
		ft_rrb(ps);
	}
}

void		sort_3_b(t_ps *ps)
{
	if (ps->b.pos > 2)
	{
		if (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 1] &&
			ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos - 2])
		{
			ft_sb(ps);
			ft_rb(ps);
			ft_sb(ps);
			ft_rrb(ps);
			ft_sb(ps);
		}
		else if (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 2] &&
			ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos - 1])
		{
			ft_sb(ps);
			ft_rb(ps);
			ft_sb(ps);
			ft_rrb(ps);
		}
		else
			sort_b(ps);
	}
	else
		sort_only_3_b(ps);
}
