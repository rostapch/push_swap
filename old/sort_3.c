/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:26:26 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:26:28 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

void	sort_only_3_a(t_ps *ps)
{
	if (ps->a.pos != 2)
		sort_2_a(ps);
	else if (ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 1] &&
		ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos - 2])
	{
		ft_sa(ps);
		ft_rra(ps);
	}
	else if (ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 2] &&
		ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos - 1])
		ft_ra(ps);
	else if (ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos] &&
		ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 2])
		ft_rra(ps);
	else if (ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos] &&
		ps->a.data[ps->a.pos] > ps->a.data[ps->a.pos - 1])
		ft_sa(ps);
	else if (ps->a.data[ps->a.pos - 1] > ps->a.data[ps->a.pos - 2] &&
		ps->a.data[ps->a.pos - 2] > ps->a.data[ps->a.pos])
	{
		ft_sa(ps);
		ft_ra(ps);
	}
}

void	sort_only_3_b(t_ps *ps)
{
	if (ps->b.pos != 2)
		sort_2_b(ps);
	else if (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 1] &&
		ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos - 2])
	{
		ft_sb(ps);
		ft_rrb(ps);
	}
	else if (ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 2] &&
		ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos - 1])
		ft_rb(ps);
	else if (ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos] &&
		ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 2])
		ft_rrb(ps);
	else if (ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos] &&
		ps->b.data[ps->b.pos] < ps->b.data[ps->b.pos - 1])
		ft_sb(ps);
	else if (ps->b.data[ps->b.pos - 1] < ps->b.data[ps->b.pos - 2] &&
		ps->b.data[ps->b.pos - 2] < ps->b.data[ps->b.pos])
	{
		ft_rb(ps);
		ft_sb(ps);
		ft_rrb(ps);
	}
}
