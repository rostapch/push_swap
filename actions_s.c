/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions_s.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:36:00 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:36:02 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int		ft_swap(t_stack *stk)
{
	int buff;

	if (!(stk && stk->pos >= 1))
		return (0);
	buff = stk->data[stk->pos];
	stk->data[stk->pos] = stk->data[stk->pos - 1];
	stk->data[stk->pos - 1] = buff;
	return (1);
}

void	ft_sa(t_ps *ps)
{
	if (ft_swap(&ps->a))
		ps->act = ft_strjoin_free(ps->act, "sa\n");
}

void	ft_sb(t_ps *ps)
{
	if (ft_swap(&ps->b))
		ps->act = ft_strjoin_free(ps->act, "sb\n");
}

void	ft_ss(t_ps *ps)
{
	if (ft_swap(&ps->b) && ft_swap(&ps->a))
		ps->act = ft_strjoin_free(ps->act, "ss\n");
}
