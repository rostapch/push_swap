# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/12 20:50:24 by rostapch          #+#    #+#              #
#    Updated: 2017/11/12 20:50:26 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror

NAME = libft

OBJFLAG = -c


SRC_C =	*.c

SRC_O = $(SRC_C:.c=.o)

all: $(NAME)

$(NAME): $(SRC_O)
	ar rcv $(addsuffix .a, $(NAME)) $(SRC_O)

.c.o:
	$(CC) $(CFLAGS) $(OBJFLAG) $< -o $@

clean:
	/bin/rm -f $(SRC_O)

fclean: clean
	/bin/rm -f $(addsuffix .a, $(NAME))

re: fclean all
.PHONY: clean all re fclean
