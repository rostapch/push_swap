# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/12 20:50:24 by rostapch          #+#    #+#              #
#    Updated: 2017/11/12 20:50:26 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CHECKER = checker
PS = push_swap

CC = gcc
CFLAGS = -Wall -Wextra -Werror
OBJFLAG = -c

INCLUDE_LIB = -L libft -l ft
HEADERS =  -I . -I libft


CH_C =	actions_p.c		actions_s.c			actions_r.c		actions_rr.c\
		check.c			checker_main.c		error.c			validate.c\
		ps_init_del.c	fill_ps.c			push_pop.c

CH_O = $(CH_C:.c=.o)


PS_C =	actions_p.c		actions_s.c			actions_r.c		actions_rr.c\
		algo.c			push_swap_main.c	error.c			validate.c\
		ps_init_del.c	fill_ps.c			push_pop.c		gnome_sort.c\
		med.c			sort_3_more.c			sort_2.c		sort_3.c\
		check.c			rot_stcs.c

PS_O = $(PS_C:.c=.o)


all: lib $(CH_O) $(PS_O)
	$(CC) $(CFLAGS) -o $(CHECKER) $(CH_O) $(INCLUDE_LIB) $(HEADERS)
	$(CC) $(CFLAGS) -o $(PS) $(PS_O) $(INCLUDE_LIB) $(HEADERS)
	
$(CHECKER): lib $(CH_O) $(CH_C)
	$(CC) $(CFLAGS) -o $(CHECKER) $(CH_O) $(INCLUDE_LIB) $(HEADERS)

$(PS): lib $(CH_O) $(CH_C)
	$(CC) $(CFLAGS) -o $(PS) $(PS_O) $(INCLUDE_LIB) $(HEADERS)

%.o: %.c
	$(CC) -c $(CFLAGS) $(HEADERS)  -o $@ $<

lib:
	make -C libft -f Makefile

lib_re:
	make re -C libft -f Makefile

lib_clean:
	make clean -C libft -f Makefile

lib_fclean:
	make fclean -C libft -f Makefile

clean: lib_clean
	/bin/rm -f $(CH_O)
	/bin/rm -f $(PS_O)

fclean: lib_fclean
	/bin/rm -f $(CH_O)
	/bin/rm -f $(PS_O)
	/bin/rm -f $(CHECKER)
	/bin/rm -f $(PS)

re: fclean all
