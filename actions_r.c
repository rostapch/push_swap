/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions_r.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 20:36:57 by rostapch          #+#    #+#             */
/*   Updated: 2017/11/12 20:36:59 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ps.h"

int		ft_rotate(t_stack *stk)
{
	int i;
	int buff;

	if (!(stk && stk->pos > 0))
		return (0);
	i = stk->pos + 1;
	buff = stk->data[stk->pos];
	while (--i > 0)
		stk->data[i] = stk->data[i - 1];
	stk->data[0] = buff;
	return (1);
}

void	ft_ra(t_ps *ps)
{
	if (ft_rotate(&ps->a))
		ps->act = ft_strjoin_free(ps->act, "ra\n");
}

void	ft_rb(t_ps *ps)
{
	if (ft_rotate(&ps->b))
		ps->act = ft_strjoin_free(ps->act, "rb\n");
}

void	ft_rr(t_ps *ps)
{
	if (ft_rotate(&ps->b) && ft_rotate(&ps->a))
		ps->act = ft_strjoin_free(ps->act, "rr\n");
}
