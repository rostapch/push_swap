/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_free.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 15:19:05 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/30 15:19:13 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin_free(char *s1, char *s2)
{
	char	*arr;
	size_t	i;

	i = -1;
	if (s1 && s2)
	{
		arr = (char*)malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2)) + 1);
		if (arr == NULL)
			return (NULL);
		while (s1[++i])
			arr[i] = s1[i];
		while (*s2)
			arr[i++] = *s2++;
		arr[i] = '\0';
		ft_strdel(&s1);
		return (arr);
	}
	return (NULL);
}
